//
//  Hello World server in C++
//  Binds REP socket to tcp://*:5555
//  Expects "Hello" from client, replies with "World"
//
#include <zmq.hpp>
#include <string>
#include <iostream>
#include <timer/clock.hh>

#include <msgpack.hpp>
#include <vector>
#include <map>

std::vector<double> controllerState;
std::vector<double> deviceState;


template <typename T>
void send(zmq::socket_t& socket, const T& data)
{

  msgpack::sbuffer packed;
  msgpack::pack(&packed, data);


  zmq::message_t body_msg(packed.size());
  std::memcpy(body_msg.data(), packed.data(), packed.size());

  //socket.send(tag_msg, ZMQ_SNDMORE);
  //neolib::hex_dump(packed.data(), packed.size(), std::cout);
  socket.send(body_msg);
}
template <typename T>
void receive(zmq::socket_t& socket, T& data)
{
  zmq::message_t body_msg;
  socket.recv(&body_msg);


  msgpack::unpacked unpacked_body;
  msgpack::unpack(&unpacked_body, static_cast<const char*>(body_msg.data()), body_msg.size());
  unpacked_body.get().convert(&data);
}
double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main () {
    
    deviceState.push_back(0.1);
    deviceState.push_back(0.2);
    
    //controllerState.push_back(0.1);
    //controllerState.push_back(0.2);
    //  Prepare our context and socket
    Clock<time_unit::us> ttclock;
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REQ);
    //zmq::socket_t socket (context, ZMQ_REP);
    //socket->bind ("ipc:///tmp/feeds/0");
    socket.connect("ipc:///tmp/test");
    long i=0;
    while (i<10000) {
        ttclock.start();
        
        deviceState[0] = fRand(0.0,10.0);
        deviceState[1] = fRand(0.0,10.0);
        //for(auto &kv: controllerState){
        //    std::cout << kv << "|";
        //}
        send(socket, deviceState);
        receive(socket, controllerState);
        ttclock.end();
        i++;
    }
    ttclock.printMean();
    return 0;
}

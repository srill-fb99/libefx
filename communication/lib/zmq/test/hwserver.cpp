//
//  Hello World server in C++
//  Binds REP socket to tcp://*:5555
//  Expects "Hello" from client, replies with "World"
//
#include <zmq.hpp>
#include <string>
#include <iostream>
#include <timer/clock.hh>

#include <msgpack.hpp>
#include <vector>
#include <map>

struct Msg{
    std::vector<double> f1;
    std::string f2;
    MSGPACK_DEFINE(f2,f1);
};

namespace neolib
{
    template<class Elem, class Traits>
    inline void hex_dump(const void* aData, std::size_t aLength, std::basic_ostream<Elem, Traits>& aStream, std::size_t aWidth = 16)
    {
        const char* const start = static_cast<const char*>(aData);
        const char* const end = start + aLength;
        const char* line = start;
        while (line != end)
        {
            aStream.width(4);
            aStream.fill('0');
            aStream << std::hex << line - start << " : ";
            std::size_t lineLength = std::min(aWidth, static_cast<std::size_t>(end - line));
            for (std::size_t pass = 1; pass <= 2; ++pass)
            {   
                for (const char* next = line; next != end && next != line + aWidth; ++next)
                {
                    char ch = *next;
                    switch(pass)
                    {
                    case 1:
                        aStream << (ch < 32 ? '.' : ch);
                        break;
                    case 2:
                        if (next != line)
                            aStream << " ";
                        aStream.width(2);
                        aStream.fill('0');
                        aStream << std::hex << std::uppercase << static_cast<int>(static_cast<unsigned char>(ch));
                        break;
                    }
                }
                if (pass == 1 && lineLength != aWidth)
                    aStream << std::string(aWidth - lineLength, ' ');
                aStream << " ";
            }
            aStream << std::endl;
            line = line + lineLength;
        }
    }
}


template <typename T>
void publish(zmq::socket_t& socket, const std::string& tag, const T& data)
{

  msgpack::sbuffer packed;
  msgpack::pack(&packed, data);


  zmq::message_t body_msg(packed.size());
  std::memcpy(body_msg.data(), packed.data(), packed.size());

  //socket.send(tag_msg, ZMQ_SNDMORE);
  neolib::hex_dump(packed.data(), packed.size(), std::cout);
  socket.send(body_msg);
}
template <typename T>
void subscribe(zmq::socket_t& socket, T& data)
{
  zmq::message_t body_msg;
  socket.recv(&body_msg);


  msgpack::unpacked unpacked_body;
  msgpack::unpack(&unpacked_body, static_cast<const char*>(body_msg.data()), body_msg.size());
  unpacked_body.get().convert(&data);
}


int main () {
    Msg msg;

    msg.f1.push_back(3.14*3.14);
    msg.f1.push_back(3.14*3.14);
    msg.f2="fourth";
    //  Prepare our context and socket
    Clock<time_unit::us> ttclock;
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REP);
    socket.bind ("tcp://*:5555");

    while (true) {
        ttclock.start();
        Msg msg;
        
        subscribe(socket, msg);
        std::cout << msg.f1[0] << " | " << msg.f2 << std::endl;
        //zmq::message_t request;
        //socket.recv (&request);


        
        //std::cout << "Received Hello" << std::endl;
        //msgpack::sbuffer sbuf;
        //msgpack::pack(sbuf, msg);
        //  Do some 'work'
        //  Send reply back to client
        publish(socket, "msg", msg);
        ttclock.end();
    }
    return 0;
}

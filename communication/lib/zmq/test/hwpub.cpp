//
//  Synchronized publisher in C++
//
// Olivier Chamoux <olivier.chamoux@fr.thalesgroup.com>


#include "zhelpers.hpp"
#include <string>
#include <iostream>

struct Msg {

};

int main () {
	zmq::context_t context(1);

    //  Socket to talk to clients
    zmq::socket_t publisher (context, ZMQ_PUB);

    int sndhwm = 0;
    publisher.setsockopt (ZMQ_SNDHWM, &sndhwm, sizeof (sndhwm));

    publisher.bind("ipc:///tmp/matlab");

    while(true){
    s_send (publisher, "test");
    }
    sleep (1);              //  Give 0MQ time to flush output
    return 0;
}


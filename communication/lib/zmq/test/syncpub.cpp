//
//  Synchronized publisher in C++
//
// Olivier Chamoux <olivier.chamoux@fr.thalesgroup.com>


#include "zhelpers.hpp"
#include <iostream>

using namespace std;

//  We wait for 10 subscribers
#define SUBSCRIBERS_EXPECTED  1

int main () {
    zmq::context_t context(1);

    //  Socket to talk to clients
    zmq::socket_t publisher (context, ZMQ_PUB);

    int sndhwm = 0;
    publisher.setsockopt (ZMQ_SNDHWM, &sndhwm, sizeof (sndhwm));

    publisher.bind("tcp://*:5561");
    //publisher.bind("ipc://pub.ipc");

    //  Socket to receive signals
    zmq::socket_t syncservice (context, ZMQ_REP);
    syncservice.bind("tcp://*:5562");
    //publisher.bind("ipc://sync.ipc");

    //  Get synchronization from subscribers
    int subscribers = 0;
    while (subscribers < SUBSCRIBERS_EXPECTED) {
        
        //  - wait for synchronization request
        s_recv (syncservice);
       
        //  - send synchronization reply
        s_send (syncservice, "");


        subscribers++;
    }
    
    cout << "OK, CONNECTED" << endl;

    //  Now broadcast exactly 1M updates followed by END
    int update_nbr;
    for (update_nbr = 0; update_nbr < 1000000; update_nbr++) {  
        s_send (publisher, "Rhubarb");
    }
    
    s_send (publisher, "END");

    sleep (1);              //  Give 0MQ time to flush output
    return 0;
}

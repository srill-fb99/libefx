//
//  Hello World client in C++
//  Connects REQ socket to tcp://localhost:5555
//  Sends "Hello" to server, expects "World" back
//
#include <zmq.hpp>
#include <string>
#include <iostream>
#include <timer/clock.hh>
#include <sml/types/types.h>

#include <msgpack.hpp>
#include <vector>
#include <map>

template <class T>
class BaseCommunication {
public : 
  virtual void send( const T& data) = 0;
  virtual void receive(T& data) = 0;
};

template <class T>
class ZMQCommunication : public BaseCommunication<T> {
private:
  zmq::context_t context;
  zmq::socket_t socket;
public: 
  ZMQCommunication(const char* ipcname) : context(1), socket(context, ZMQ_REP){
    socket.bind(ipcname); // ipcname = "ipc:///tmp/test"
  }
  virtual void send( const T& data){
    msgpack::sbuffer packed;
    msgpack::pack(&packed, data);
    zmq::message_t body_msg(packed.size());
    std::memcpy(body_msg.data(), packed.data(), packed.size());
    socket.send(body_msg); 
  }
  virtual void receive(T& data){
    zmq::message_t body_msg;
    socket.recv(&body_msg);
    msgpack::unpacked unpacked_body;
    msgpack::unpack(&unpacked_body, static_cast<const char*>(body_msg.data()), body_msg.size());
    unpacked_body.get().convert(&data);
  }
};

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}


std::vector<double> deviceState = std::vector<double>(INPUT::NUMBER,0.0);
std::vector<double> controllerState = std::vector<double>(OUTPUT::NUMBER,0.0);


int main ()
{
    //controllerState.push_back(0.05);
    //controllerState.push_back(0.02);

    ZMQCommunication <std::vector<double>>comm("ipc:///tmp/test");

    while (true){
      
      // controllerState[0] += 0.1; //fRand(0.0,10.0);
      // controllerState[1] = fRand(0.0,10.0);
      comm.receive( controllerState );
      comm.send( deviceState );
    }

    return 0;
}



# Simple communication test between matlab-simulink and a small cpp program


This can be used as a sample for the creation of more complex interaction. 
A small issue still exists in the simulink model. It is not possible for now to build the model for external use. 


## Usage 


### Installation 


1. Compilation of cpp_device 

   go to the folder cpp_device and launch the make script 

	./make

2. Compile the sfunction 

   go to the folder simulink_controller and launch the following command 

	mex -lzmq -I/home/efx/.local/usr/include/spinalDynamics/ -L/home/efx/.local/usr/lib/spinalDynamics -ldynamics -lsetting-manager -ltimer -lyaml-cpp -lmsgpack -lpthread -lm -lrt -lcrypt -L. -lcrypt sfunc.cpp

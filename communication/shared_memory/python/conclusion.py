# Python modules
import time
import mmap
import os
import sys
PY_MAJOR_VERSION = sys.version_info[0]
# hashlib is only available in Python >= 2.5. I still want to support 
# older Pythons so I import md5 if hashlib is not available. Fortunately
# md5 can masquerade as hashlib for my purposes.
try:
    import hashlib
except ImportError:
    import md5 as hashlib

# 3rd party modules
import posix_ipc

# Utils for this demo
import utils


PY_MAJOR_VERSION = sys.version_info[0]

bdc = utils.BiDirectionalCommunication(1);

utils.say("Oooo 'ello, I'm Mrs. Conclusion!")

what_i_wrote = ""
for i in range(0, bdc.params["ITERATIONS"]):
    utils.say("iteration %d" % i)
    s = bdc.channel.read();

    what_i_wrote = "hey";

    bdc.channel.write(what_i_wrote)

# semaphore.close()
# mapfile.close()

utils.say("")
utils.say("%d iterations complete" % (i + 1))

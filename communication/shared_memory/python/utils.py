import time 
import sys
import posix_ipc
import os
import mmap

PY_MAJOR_VERSION = sys.version_info[0]

if PY_MAJOR_VERSION > 2:
    NULL_CHAR = 0
else:
    NULL_CHAR = '\0'

def raise_error(error, message):
    # I have to exec() this code because the Python 2 syntax is invalid
    # under Python 3 and vice-versa.
    s = "raise "
    s += "error, message" if (PY_MAJOR_VERSION == 2) else "error(message)" 
        
    exec(s)


def say(s):
    """Prints a timestamped, self-identified message"""
    who = sys.argv[0]
    if who.endswith(".py"):
        who = who[:-3]
        
    s = "%s@%1.6f: %s" % (who, time.time(), s)
    #print (s)


def write_to_memory(mapfile, s):
    """Writes the string s to the mapfile"""
    say("writing %s " % s)
    mapfile.seek(0)
    # I append a trailing NULL in case I'm communicating with a C program.
    s += '\0'
    if PY_MAJOR_VERSION > 2:
        s = s.encode()

    # import ipdb; ipdb.set_trace()
    mapfile.write(s)


def read_from_memory(mapfile):
    """Reads a string from the mapfile and returns that string"""
    mapfile.seek(0)
    s = [ ]
    c = mapfile.read_byte()
    while c != NULL_CHAR:
        s.append(c)
        c = mapfile.read_byte()
            
    if PY_MAJOR_VERSION > 2:
        s = [chr(c) for c in s]
    s = ''.join(s)
    
    say("read %s" % s)
    
    return s


def read_params():
    """Reads the contents of params.txt and returns them as a dict"""
    params = { }
    
    f = open("params.txt")
    
    for line in f:
        line = line.strip()
        if line:
            if line.startswith('#'):
                pass # comment in input, ignore
            else:
                name, value = line.split('=')
                name = name.upper().strip()
            
                if name == "PERMISSIONS":
                    # Think octal, young man!
                    value = int(value, 8)
                elif "NAME" in name:
                    # This is a string; leave it alone.
                    pass
                else:
                    value = int(value)
                
                #print "name = %s, value = %d" % (name, value)
            
                params[name] = value

    f.close()
    
    return params
    

class BiDirectionalCommunication:

    def __init__(self, type_id = 0): # 0 : write first, 1 : read first
        self.params = read_params()

        if type_id <= 1:
            # Create the shared memory and the semaphore.
            memory = posix_ipc.SharedMemory(self.params["SHARED_MEMORY_NAME"], posix_ipc.O_CREAT,
                                            size=self.params["SHM_SIZE"])
            semaphore1 = posix_ipc.Semaphore(self.params["SEMAPHORE_NAME1"], posix_ipc.O_CREAT)
            semaphore2 = posix_ipc.Semaphore(self.params["SEMAPHORE_NAME2"], posix_ipc.O_CREAT)

            # MMap the shared memory
            mapfile = mmap.mmap(memory.fd, memory.size)

            
            if type_id == 0:
                self.channel = Channel(mapfile,semaphore1,semaphore2)
            else:
                self.channel = Channel(mapfile,semaphore2,semaphore1)
        
            os.close(memory.fd)
        

    def clean(self):
        say("Destroying semaphore and shared memory.")
        try:
            self.channel.mapfile.close()
            self.channel.semaphore1.close()
            self.channel.semaphore2.close()
        except:
            pass

        try:
            posix_ipc.unlink_shared_memory(self.params["SHARED_MEMORY_NAME"])
            s = "memory segment %s removed" % self.params["SHARED_MEMORY_NAME"]
            print (s)
        except:
            print ("memory doesn't need cleanup")
            
            
        try:
            posix_ipc.unlink_semaphore(self.params["SEMAPHORE_NAME1"])
            s = "semaphore %s removed" % self.params["SEMAPHORE_NAME1"]
            print (s)
        except:
            print ("semaphore doesn't need cleanup")

        try:
            posix_ipc.unlink_semaphore(self.params["SEMAPHORE_NAME2"])
            s = "semaphore %s removed" % self.params["SEMAPHORE_NAME2"]
            print (s)
        except:
            print ("semaphore doesn't need cleanup")



class Channel:
    def __init__(self,mapfile,semaphore1,semaphore2):
        # Once I've mmapped the file descriptor, I can close it without 
        # interfering with the mmap. 
        self.semaphore1 = semaphore1
        self.semaphore2 = semaphore2
        self.mapfile = mapfile

    def write(self, string):
        write_to_memory(self.mapfile, string)
        self.semaphore1.release()

    def read(self):
        self.semaphore2.acquire()
        return read_from_memory(self.mapfile)



#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h> 
#include <sys/sem.h>
#include <iostream>
#include <semaphore.h>
#include <timer/clock.hh>

using namespace std;

int main(int argc, char *argv[])
{
    key_t key;
    int shmid;
    int *data;
    int val;
    Clock<time_unit::ns> ttclock;

    sem_t *semaphore1;
    sem_t *semaphore2;
    if ((semaphore1 = sem_open(argv[1], O_CREAT, 0644, 0)) == SEM_FAILED ) {
        perror("sem_open");
        exit(EXIT_FAILURE);
    }
    if ((semaphore2 = sem_open(argv[2], O_CREAT, 0644, 0)) == SEM_FAILED ) {
        perror("sem_open");
        exit(EXIT_FAILURE);
    }
    /*
     Get Sharde memory segements
     */
    key = ftok(".sharedmemory", 'R');
    shmid = shmget(key, 1024, 0644 | IPC_CREAT);
    data = (int*)shmat(shmid, (void *)0, 0);
                    /* l_type   l_whence  l_start  l_len  l_pid   */
    sem_getvalue(semaphore1,&val);
    cout << "sem1=" << data[0] << "|" << val << endl;
    sem_getvalue(semaphore2,&val);
    cout << "sem2=" << data[1] << "|" << val << endl << endl;

    while(data[0] != 0){
         sem_wait(semaphore1);
         data[0]--;
    }
    while(data[1] != 0){
         sem_wait(semaphore2);
         data[1]--;
    }
    cout << endl << "after" << endl;

    sem_getvalue(semaphore1,&val);
    cout << "sem1=" << data[0] << "|" << val << endl;
    sem_getvalue(semaphore2,&val);
    cout << "sem2=" << data[1] << "|" << val << endl << endl;

    // while(true){
    //     sem_getvalue(semaphore2,&val);
    //     cout << "semaphore2 : "  << val << endl;
    //     if(val > 0)
    //         sem_wait(semaphore2);
    //     else
    //         break;
    // }


    int i=0;
    for(;i<100000;i++){
        ttclock.start();
        data[1]++;
        sem_post(semaphore2);
        sem_wait(semaphore1);
        data[0]--;
        /*
         Lock it
         */
        // fl.l_type = F_RDLCK;
        // if (fcntl(fd, F_SETLKW, &fl) == -1) {
        //     perror("fcntl");
        //     exit(1);
        // }
        
        /* Do stuff */
        
        //for ( int j=0;j<100;j++)
        

        cout << "sem1=" << data[0] << endl;
        cout << "sem2=" << data[1] << endl << endl;


        /*
         Unlock it
         */

        // fl.l_type = F_UNLCK;  /* set to unlock same region */

        // if (fcntl(fd, F_SETLK, &fl) == -1) {
        //     perror("fcntl");
        //     exit(1);
        // }

        ttclock.end();   
        
    }
    cout << data[0] << endl;

    ttclock.printMean();
    printf("Bye.\n");


    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h> 
#include <sys/sem.h>
#include <iostream>
#include <semaphore.h>
#include <timer/clock.hh>

using namespace std;

int main(int argc, char *argv[])
{
    key_t key;
    int shmid;
    int *data;
    int val;
    
                    /* l_type   l_whence  l_start  l_len  l_pid   */
    sem_t *semaphore1;
    sem_t *semaphore2;
    if ((semaphore1 = sem_open(argv[1], O_CREAT, 0644, 0)) == SEM_FAILED ) {
        perror("sem_open");
        exit(EXIT_FAILURE);
    }
    if ((semaphore2 = sem_open(argv[2], O_CREAT, 0644, 0)) == SEM_FAILED ) {
        perror("sem_open");
        exit(EXIT_FAILURE);
    }

    /*
     Get Sharde memory segements
     */
    key = ftok(".sharedmemory", 'R');
    shmid = shmget(key, 1024, 0644 | IPC_CREAT);
    data = (int*)shmat(shmid, (void *)0, 0);
    
    sem_getvalue(semaphore1,&val);
    data[0] = val;
    sem_getvalue(semaphore2,&val);
    data[1] = val;



    return 0;
}

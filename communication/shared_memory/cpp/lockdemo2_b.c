#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h> 
#include <iostream>
#include <semaphore.h>

using namespace std;

int main(int argc, char *argv[])
{
    key_t key;
    int shmid;
    int *data;

    sem_t *semaphore1;
    sem_t *semaphore2;
    if ((semaphore1 = sem_open(argv[1], O_CREAT, 0644, 0)) == SEM_FAILED ) {
        perror("sem_open");
        exit(EXIT_FAILURE);
    }
    if ((semaphore2 = sem_open(argv[2], O_CREAT, 0644, 0)) == SEM_FAILED ) {
        perror("sem_open");
        exit(EXIT_FAILURE);
    }
    /*
     Get Sharde memory segements
     */
    key = ftok(".sharedmemory", 'R');
    shmid = shmget(key, 1024, 0644 | IPC_CREAT);
    data = (int*)shmat(shmid, (void *)0, 0);
                    /* l_type   l_whence  l_start  l_len  l_pid   */


    int i=0;
    for(;i<100000;i++){
        sem_wait(semaphore2);
        data[1]--;
        //usleep(100);
        // fl.l_type = F_RDLCK;


        // /*
        //  Lock it
        //  */
        // if (fcntl(fd, F_SETLKW, &fl) == -1) {
        //     perror("fcntl");
        //     exit(1);
        // }

        /* Do stuff */

        //cout << data[0] << endl;

        /*
         Unlock it
         */

        // fl.l_type = F_UNLCK;  /* set to unlock same region */

        // if (fcntl(fd, F_SETLK, &fl) == -1) {
        //     perror("fcntl");
        //     exit(1);
        // }
        data[0]++;
        sem_post(semaphore1);
    }

    printf("Bye.\n");

    return 0;
}
